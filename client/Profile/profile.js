
ContractAddress = "0x552996402B1a1030C8eE50a9cD54b9f407Ad22AA";

Template.profile.helpers({
  isAuthor() {
    var userType=   Session.get("userType");
    var isAuthor = false;
    if(1==userType){
      isAuthor = true;
    }
    return isAuthor;
  },
  isPubisher() {
    var userType=   Session.get("userType");
    var isPubisher = false;
    if(0==userType){
      isPubisher = true;
    }
    return isPubisher;
  }
});


Template.solidity.helpers({
  initializeBlance() {
    // addProject();
  },
  freelancerAddress() {
    return Session.get("freelancerAddress");
  },

});

Template.solidity.events({
  'click .addProject': function(event) {
    event.preventDefault();
    var pid = $("#projectId").val();
    addProject(pid);
  },
  'click .getFreelancer': function(event) {
    event.preventDefault();
    var pid = $("#projectIdFreelancer").val();
    getFreelancerAddress(pid);
  },
  'click .assignFreelancer': function(event) {
    event.preventDefault();
    var pid = event.target.projectId.value;
    var freelancerAddr = event.target.freelancerAddr.value;
    addNewFreelancer(pid,freelancerAddr);
  },
});


function getFreelancerAddress(_ProjectId){
  myContract =  getBlanceContract();
  console.log("getFreelancerAddress _ProjectId " + _ProjectId);

  myContract.getFreelancer(_ProjectId,function(error,result){
    console.log("getFreelancerAddress result " + result);

    Session.set("freelancerAddress",result);
  });
}


function addNewFreelancer(_ProjectId,_FreelancerAddress){

  console.log("_FreelancerAddress " + _FreelancerAddress);
  console.log("_ProjectId " + _ProjectId);

  myContract =  getBlanceContract();

  myContract.addNewFreelancer(
    _ProjectId,
    _FreelancerAddress,
    function (e, contract){
      console.log(e, contract);
      if (typeof contract.address !== 'undefined') {
        console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
      }
    });
  }

  function addProject(_ProjectId){

    var blockchain = web3.eth;
    var defaultAccount = blockchain.defaultAccount;
    console.log("blockchain " + defaultAccount);
    console.log("_ProjectId " + _ProjectId);

    myContract =  getBlanceContract();

    myContract.addProject(
      _ProjectId,
      {
        from: web3.eth.accounts[0],
        value:10000000000000000,//0.01 eth
        data: '0x6060604052600080546c0100000000000000000000000033810204600160a060020a03199091161790556102d6806100376000396000f3606060405236156100615760e060020a60003504630be846a48114610066578063107046bd1461008a5780632be76047146100c8578063485889b11461014d578063571836981461019b578063a424792f14610237578063f6e3fb061461024e575b610002565b3461000257610279600435600081815260016020819052604090912001545b919050565b346100025760016020819052600435600090815260409020805491810154600282015460039092015461028b9392600160a060020a03908116911684565b3461000257610235600435600081815260016020819052604082200154908111156101495760008281526001602081905260408083209182018390556002909101549051600160a060020a039091169183156108fc02918491818181858888f193505050501515610149576000828152600160208190526040909120018190555b5050565b34610002576004356000908152600160205260409020600201805473ffffffffffffffffffffffffffffffffffffffff19166c01000000000000000000000000602435810204179055610235565b60408051608081018252600435808252346020808401918252600084860181815233606087019081529482526001928390529590209351845590518382015592516002808401805473ffffffffffffffffffffffffffffffffffffffff199081166c010000000000000000000000009485028590041790915592516003909401805490931693820291909104929092179055805490910190555b005b34610002576102ba600054600160a060020a031681565b34610002576102ba600435600081815260016020526040902060020154600160a060020a0316610085565b60408051918252519081900360200190f35b604080519485526020850193909352600160a060020a0391821684840152166060830152519081900360800190f35b60408051600160a060020a039092168252519081900360200190f3',
        gas: '4700000'
      }, function (e, contract){
        console.log(e, contract);
        if (typeof contract.address !== 'undefined') {
          console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
        }
      });
    }

    function getBlanceContract(){

      var blanceContract = web3.eth.contract([{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"}],"name":"getProjectCost","outputs":[{"name":"projectCost","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"projects","outputs":[{"name":"projectId","type":"uint256"},{"name":"projectCost","type":"uint256"},{"name":"freelancerAddress","type":"address"},{"name":"ownerAddress","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"}],"name":"payToFreelancer","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"},{"name":"_FreelancerAddress","type":"address"}],"name":"addNewFreelancer","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_ProjectId","type":"uint256"}],"name":"addProject","outputs":[],"payable":true,"type":"function"},{"constant":true,"inputs":[],"name":"blanceCreator","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"}],"name":"getFreelancer","outputs":[{"name":"freelancerAddress","type":"address"}],"payable":false,"type":"function"},{"inputs":[],"type":"constructor","payable":true}]).at(ContractAddress);;
      // myContract =  blockchain.contract(ABIArray).at(ContractAddress);
      return blanceContract;
    }
