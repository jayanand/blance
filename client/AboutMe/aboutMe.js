import { Gigs } from '../../common/api/api.js';
import { Users } from '../../common/api/api.js';
var userObject;
Session.set("aboutmeProjectGigState", 3);



Template.aboutme.helpers({
  getId() {

    var userIdd = FlowRouter.getParam("userId");

    userObject = Users.find({ userId:userIdd}).fetch()[0];

    if (typeof userObject === "undefined") {
      // logMeOut();
    }else{
      console.log('Name '+userObject.userName);

      Session.set("aboutmeUserName", userObject.userName);
      Session.set("aboutmeEmail", userObject.email);
      Session.set("aboutmeUserType", userObject.userType);
      Session.set("aboutmeUserId", userObject.userId);
    }

    console.log("UserIds " + userIdd);
    // return userIdd;
  }
});


Template.aboutmeHead.helpers({
  getName() {
    return Session.get("aboutmeUserName");
  }
});
Template.aboutmeContact.helpers({
  getName() {
    return Session.get("aboutmeUserName");
  },
  getLocation() {
    return Session.get("aboutmeLocation");
  },getEmail() {
    return Session.get("aboutmeEmail");
  }
});
Template.aboutmeAbout.helpers({
  getName() {
    return Session.get("aboutmeUserName");
  },
  getLocation() {
    return Session.get("aboutmeLocation");
  },getEmail() {
    return Session.get("aboutmeEmail");
  }
});



Template.aboutmePortfolio.helpers({
  filteredAuthorGigs() {

    var userId = FlowRouter.getParam("userId");
    var userType =  Session.get("aboutmeUserType");

    var searchCriteria = Session.get("aboutmeProjectGigState") || {};

    if(userType == 1){
      console.log("AuthorId " + userId + " searchCriteria " + searchCriteria);

      if(searchCriteria == 1){
        return Gigs.find({ "projectState": searchCriteria }, { sort: { createdAt: -1 } });
      } else if(searchCriteria >= 0){
        return Gigs.find({ gigFreelancer: userId, "projectState": searchCriteria }, { sort: { createdAt: -1 } });
      }else{
        return Gigs.find({ gigFreelancer: userId }, { sort: { createdAt: -1 } });
      }
    }else{
      console.log("PublisherId " + userId + " searchCriteria " + searchCriteria);

      if(searchCriteria == 1){
        return Gigs.find({ "projectState": searchCriteria }, { sort: { createdAt: -1 } });
      } else if(searchCriteria >= 0){
        return Gigs.find({ gigOwner: userId, "projectState": searchCriteria }, { sort: { createdAt: -1 } });
      }else{
        return Gigs.find({ gigOwner: userId }, { sort: { createdAt: -1 } });
      }
    }
  },
});



//1 for open state, 2 for approval state, 3 for approved state,4 delevered,5 accepted
Template.aboutmeHead.events({

  'click .author-waiting': function(event) {
    event.preventDefault();
    Session.set("aboutmeProjectGigState", 2);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-waiting").addClass("active");
  },
  'click .author-approved': function(event) {
    event.preventDefault();
    Session.set("aboutmeProjectGigState", 3);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-approved").addClass("active");
  },
  'click .author-completed': function(event) {
    event.preventDefault();
    Session.set("aboutmeProjectGigState", 5);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-completed").addClass("active");
  },

});
