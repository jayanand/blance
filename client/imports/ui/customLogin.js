import './customLogin.html';
import { Users } from '../../../common/api/api.js';

Template.signup.events({
  'submit form': function(event) {
    event.preventDefault();
    var emailVar = event.target.signupEmail.value;
    var passwordVar = event.target.signupPassword.value;
    var signupName = event.target.signupName.value;
    var userType = event.target.userType.value;
    var username = Random.id();

    Accounts.createUser({
      username: username,
      email: emailVar,
      password: passwordVar
    },function(err) {
      if (err)
      console.log(err);

      console.log("Signup Successfull ");
      // AccountStatus to maintain account status
      // 1. UnVerified - this is when the account holders sign's up
      // 2. Verified - this is when the User is verified
      // 3. Blocked - this is when the User is reported spam
      // 4. Deleted - this is user requests to get deleted

      Users.insert({
        userName:  signupName,
        email:   emailVar,
        userType:  userType,
        userId: Meteor.userId(),
        createdAt: new Date(), // current time
        lastLogin: new Date(), // current time
        AccountStatus:0,
        accountAddress:"",
        totalSpendings:0,
        totalEarnings:0,
        crediblityFactor:0,
        totalGigsSubmitted :0,
        totalGigsDelivered:0,
        description:"",
        profilePicUrl:"http://santetotal.com/wp-content/uploads/2014/05/default-user.png",
        country:"",
        timeZone:"",
        interests:"Any",
        walletAddress:"",

      },function(err,records){

        if (err)
        console.log("signup error "+err);

        console.log("Record added as "+records);

        fillUserDetails();
        reRoute();
      });
    });
  }
});
Template.login.events({
  'submit form': function(event) {
    event.preventDefault();
    var emailVar = event.target.loginEmail.value;
    var passwordVar = event.target.loginPassword.value;
    Meteor.loginWithPassword(emailVar, passwordVar,function(err) {
      console.log("LOGIN");

      if (err)
      console.log(err);
      else{
        fillUserDetails();
        reRoute();
      }
    });
  }
});

Accounts.onLogin(function() {
  reRoute();
});

function reRoute(){
  fillUserDetails();
  console.log('reRoute redirecting! again');

  var path = FlowRouter.current().path;
  // we only do it if the user is in the login page
  console.log('reRoute redirecting!');

  if(path === "/PublisherLogin"){
    FlowRouter.go("/Publisher");
  }
  if(path === "/AuthorLogin"){
    FlowRouter.go("/Author");
  }
  if(path === "/Login"){
    FlowRouter.go("/");
  }
  if(path === "/Signup"){
    FlowRouter.go("/");
  }
  // if(path === "/"){
  //   FlowRouter.go("/Profile");
  // }
}

function fillUserDetails(){
  var userObject = Users.find({ userId:Meteor.userId()}).fetch()[0];

  console.log('fillUserDetails '+userObject);
  if (typeof userObject === "undefined") {
    // logMeOut();
  }else{
    Session.set("userName", userObject.userName);
    Session.set("email", userObject.email);
    Session.set("userType", userObject.userType);
    Session.set("userId", userObject.userId);
    Session.set("profilePicUrl", userObject.profilePicUrl);

  }

}
// This is for logout
Template.publisherNav.events({
  'click .logout': function(event) {
    event.preventDefault();
    logMeOut();
  }
});

function logMeOut(){
  Meteor.logout();
  Session.set("userName", "");
  Session.set("userEmail", "");
  Session.set("userType", -1);
  Session.set("userId", "");
  FlowRouter.go("/Login");
}
