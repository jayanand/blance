import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Gigs } from '../common/api/api.js';

import './imports/startup/accounts-configuration.js';
import './imports/ui/onlyIfLoggedIn.js';
import './imports/ui/customLogin.js';
import './Home/homeGigs.js';

import './main.html';

// smooth scroll
$(document).on('click', 'a', function(event){
  $('html, body').animate({
    scrollTop: $( $.attr(this, 'href') ).offset().top
  }, 500);
});



// var defaultAccount = web3.eth.defaultAccount;
// console.log(defaultAccount); // ''
//
// // set the default account
// web3.eth.defaultAccount = '0x8888f1f195afa192cfee860698584c030f4c9db1';
