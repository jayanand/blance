import { Gigs } from '../../common/api/api.js';
import { Users } from '../../common/api/api.js';

ContractAddress = "0xa73fEB64eaF4b10B9140a45D14980cb5E6bED24D";

Session.set("publisherProjectGigState", 2);

Template.publisherGig.events({
  'click .approveButton': function(event) {
    event.preventDefault();
    // Meteor.call(accordations);
    // only logged in user would have this
    if(!!Meteor.user()){
      var userObject = Users.find({ userId:this.gigFreelancer}).fetch()[0];
      $("#acceptModalTitle").text(userObject.userName);
      $("#acceptModalDescription").text(userObject.description);
      $("#crediblityFactor").text(userObject.crediblityFactor);
      $("#totalGigsDelivered").text(userObject.totalGigsDelivered);
      $("#totalEarnings").text(userObject.totalEarnings);
      $("#walletAddress").text(userObject.walletAddress);
      $("#acceptModalVisitProfile").attr("href", "/aboutme/"+userObject.userId);


      $(".confirm-approve").attr("id",""+this._id);
      $(".confirm-decline").attr("id",""+this._id);

    }

    $("#approveModal").show();
  },
  'click .reviewButton': function(event) {
    event.preventDefault();

    if(!!Meteor.user()){
      $("#acceptModalContent").text(this.gigContent);
      $(".confirm-reviewed").attr("id",""+this._id);
    }

    $("#reviewModal").show();
  },
  'click .contentButton': function(event) {
    event.preventDefault();
    console.log("contentButton - ");
    if(!!Meteor.user()){
      $("#contentModalContent").text(this.gigContent);
    }

    $("#gigContentModal").show();
  },

});

Template.gigApproveModal.events({
  'click .confirm-approve': function(event) {
    event.preventDefault();
    $('#approveModal').hide();
    var gigId =$(".confirm-approve").attr("id");
    console.log("id - " + gigId);
    var gigObject = Gigs.find({_id:gigId}).fetch()[0];
    console.log("obj "+gigObject);
    Gigs.update(gigId, {
      $set: {
        projectState: 3
      },
    });
    addFreelancer(gigObject.projectId,"0xD1a45f60A6e7ec2c9D02f656986E770bb0bDc577");
    $('#approveModalAction').show();
  },
  'click .confirm-decline': function(event) {
    event.preventDefault();
    $('#approveModal').hide();
    var gigId =$(".confirm-decline").attr("id");
    console.log("id - " + gigId);

    Gigs.update(gigId, {
      $set: {
        projectState: 1,
        gigFreelancer:"",
      },
    });
    // $('#approveModalAction').show();
  },
});


// this is to release payment
Template.gigReviewModal.events({
  'click .confirm-reviewed': function(event) {
    event.preventDefault();
    $('#reviewModal').hide();
    var gigId =$(".confirm-reviewed").attr("id");
    console.log("id - " + gigId);

    // Gigs.update(gigId, {
    //   $set: {
    //     projectState: 5
    //   },
    // });
    var gigObject = Gigs.find({_id:gigId}).fetch()[0];


payFreelancer(gigObject.projectId);
    $('#releasePayment').show();
  },
});


Template.publisherGig.helpers({
  isWaitingForApproval() {
    var isWaitingForApproval = false;
    if(2== this.projectState){
      isWaitingForApproval = true;
    }
    return isWaitingForApproval;
  },
  isCompleted() {
    var isCompleted = false;
    if(4== this.projectState){
      isCompleted = true;
    }
    return isCompleted;
  },
  isReviewed() {
    var isReviewed = false;
    if(5 == this.projectState){
      isReviewed = true;
    }
    return isReviewed;
  },
});

Template.publisherNav.helpers({
  currentUser: function() {
    console.log(Meteor.userId());
    return Meteor.userId();
  }
})

Template.publisherPortfolio.helpers({
  filteredPublisherGigs() {
    var searchCriteria = Session.get("publisherProjectGigState") || {};
    console.log("searchCriteria " + searchCriteria);
    if(searchCriteria >= 0){
      return Gigs.find({ gigOwner: Meteor.userId(), "projectState": searchCriteria }, { sort: { createdAt: -1 } });
    }else{
      return Gigs.find({ gigOwner: Meteor.userId() }, { sort: { createdAt: -1 } });
    }
  },
});

//1 for open state, 2 for approval state, 3 for approved state,4 delevered,5 accepted
Template.publisherHead.events({
  'click .publisher-all': function(event) {
    event.preventDefault();
    Session.set("publisherProjectGigState", -1);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".publisher-all").addClass("active");
  },
  'click .publisher-waiting': function(event) {
    event.preventDefault();
    Session.set("publisherProjectGigState", 2);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".publisher-waiting").addClass("active");
  },
  'click .publisher-approved': function(event) {
    event.preventDefault();
    Session.set("publisherProjectGigState", 3);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".publisher-approved").addClass("active");
  },
  'click .publisher-completed': function(event) {
    event.preventDefault();
    Session.set("publisherProjectGigState", 4);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".publisher-completed").addClass("active");
  },
  'click .publisher-open': function(event) {
    event.preventDefault();
    Session.set("publisherProjectGigState", 1);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".publisher-open").addClass("active");

  },
  'click .publisher-accepted': function(event) {
    event.preventDefault();
    Session.set("publisherProjectGigState", 5);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".publisher-accepted").addClass("active");

  },
});




Template.publisherNav.events({
  'click .logout': function(event) {
    event.preventDefault();
    Meteor.logout();
  },
  // 'click .publish-new-gig': function(event) {
  //   event.preventDefault();
  //   FlowRouter.go("/GigForm");
  // }
});


function isEmpty(str) {
  return (!str || 0 === str.length);
}

Template.publisherNav.helpers({
  userName() {

    // // console.log("updating gig status");
    // Gigs.update("BimzosTikfoWCAwMB", {
    //   $set: {
    //     projectState: 2
    //   },
    // });

    return Session.get("userName");
  },
  userEmail() {
    return Session.get("email");
  },
  userPic() {
    var profileP = Session.get("profilePicUrl");

    if(isEmpty(profileP)){
      profileP= "http://santetotal.com/wp-content/uploads/2014/05/default-user.png";
    }
    return profileP;
  },
  initializeContract(){

  }
});


function addFreelancer(_ProjectId, _FreelancerAddress){

  var blockchain = web3.eth;
  var defaultAccount = blockchain.defaultAccount;

  console.log("blockchain " + defaultAccount);
  console.log("_ProjectId " + _ProjectId);
  console.log("_FreelancerAddress " + _FreelancerAddress);

  myContract =  getBlanceContract();

  myContract.addNewFreelancer(
    _ProjectId,
    _FreelancerAddress,
    function (e, contract){
      console.log(e, contract);
      if (typeof contract.address !== 'undefined') {
        console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
      }
    });
  }

  function payFreelancer(_ProjectId){

    var blockchain = web3.eth;
    var defaultAccount = blockchain.defaultAccount;

    console.log("blockchain " + defaultAccount);
    console.log("_ProjectId " + _ProjectId);

    myContract =  getBlanceContract();

    myContract.payToFreelancer(
      _ProjectId,
      {
        data: '0x606060405236156100985760e060020a60003504630b9364d8811461009d578063107046bd146100e35780632be760471461012d57806331474e751461025a5780633d3aa81c14610287578063485889b11461030457806353339ba014610333578063571836981461037a578063a424792f14610450578063af9bdbe814610467578063c1b9745314610494578063f664ceb31461053f575b610002565b34610002576003602081905260043560009081526040902080546001820154600283015492909301546105c193919260ff8316926101009004600160a060020a03169185565b3461000257600160208190526004356000908152604090208054918101546002820154600392909201546105f5939260ff811691610100909104600160a060020a03908116911685565b3461000257610302600435600081815260016020819052604082206002015460ff1614156100985750600081815260016020819052604082200154908111156106b457604060008181208482526001602081905281018290559151600292909201546101009004600160a060020a03169183156108fc0291849190818181858888f193505050501515610649578060016000506000848152602001908152602001600020600050600101600050819055507fa09e6165f4dcdc4e418edf5d4543c54cdb7d7a2166ff3723d856102f4f4a3c4d6001600050600084815260200190815260200160002060005060020160019054906101000a9004600160a060020a0316826040518083600160a060020a031681526020018281526020019250505060405180910390a16106b4565b3461000257610302600435600081815260016020526040902060029081015460ff1614156106b857610002565b34610002576040805160a0810182526004358082526000602083810182815233858701908152606086018481526080870185815295855260039384905296909320945185556001850180549151935160ff199092169390931761010060a860020a031916610100919091021790915592516002830155519101555b005b346100025761030260043560243560008281526001602052604090206002015460ff166004146106d657610002565b3461000257600260208190526004356000908152604090209081015481546001830154600393909301546105c193919260ff8316926101009004600160a060020a03169185565b6103026004356040805160a081018252828152346020828101828152600184860181815260006060870181815233608089019081528a83528487529189902097518855935192870192909255600286018054915193516101000260ff19929092169390931761010060a860020a0319161790915560039093018054935173ffffffffffffffffffffffffffffffffffffffff199094169390931790925582518481529182015281517f0e7c3051efa8662a562a2e57ddc9d8bc96de88752d421344ee2bdd05d5fb62b0929181900390910190a150565b346100025761062c600054600160a060020a031681565b3461000257610302600435600081815260016020526040902060029081015460ff16141561070357610002565b34610002576040805160a08101825260043580825260243560208381019182526044358486019081526064356060860190815260843560808701908152600095865260019384905296909420945185559151908401556002830180549251915160ff19939093169290921761010060a860020a031916610100919091021790556003018054915173ffffffffffffffffffffffffffffffffffffffff19909216919091179055610302565b34610002576103026004356040805160a081018252828152600060208281018281523384860190815260608501848152608086018581528886526002948590529690942094518555600185018054925191516101000260ff199093169190911761010060a860020a031916919091179055905190820155905160039091015550565b604080519586526020860194909452600160a060020a03929092168484015260608401526080830152519081900360a00190f35b60408051958652602086019490945284840192909252600160a060020a039081166060850152166080830152519081900360a00190f35b60408051600160a060020a03929092168252519081900360200190f35b6000828152600160209081526040918290206002908101805460ff1916909117908190558251610100909104600160a060020a0316815290810183905281517f4f77fdddb7617c292c9b97ad8505c3cb17726a2636b8bc4a46dc98d6d62c531c929181900390910190a15b5050565b6000908152600160205260409020600201805460ff19166003179055565b60008281526001602052604090206002018054610100830261010060a860020a0319919091161790555050565b6000908152600160205260409020600201805460ff1916600417905556',
        gas: '650540000000000'
      },
      function (e, contract){
        console.log(e, contract);
        if (typeof contract.address !== 'undefined') {
          console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
        }
      });
    }

    function getBlanceContract(){

      var blanceContract = web3.eth.contract([{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"freelancers","outputs":[{"name":"freelancerId","type":"uint256"},{"name":"accountStatus","type":"uint8"},{"name":"freelancerAddress","type":"address"},{"name":"totalEarnings","type":"uint256"},{"name":"crediblityFactor","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"projects","outputs":[{"name":"projectId","type":"uint256"},{"name":"projectCost","type":"uint256"},{"name":"projectStatus","type":"uint8"},{"name":"freelancerAddress","type":"address"},{"name":"ownerAddress","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"}],"name":"payToFreelancer","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"}],"name":"projectWithdrawen","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_LancerId","type":"uint256"}],"name":"freelancerSignup","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"},{"name":"_FreelancerAddress","type":"address"}],"name":"addNewFreelancer","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"projectOwners","outputs":[{"name":"projectOwnerId","type":"uint256"},{"name":"accountStatus","type":"uint8"},{"name":"accountAddress","type":"address"},{"name":"totalSpendings","type":"uint256"},{"name":"crediblityFactor","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_ProjectId","type":"uint256"}],"name":"addProject","outputs":[],"payable":true,"type":"function"},{"constant":true,"inputs":[],"name":"blanceCreator","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_projectId","type":"uint256"}],"name":"projectCancelled","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_ProjectId","type":"uint256"},{"name":"_ProjectCost","type":"uint256"},{"name":"_ProjectStatus","type":"uint8"},{"name":"_FreelancerAddress","type":"address"},{"name":"_OwnerAddress","type":"address"}],"name":"assignProject","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_OwnerId","type":"uint256"}],"name":"projectOwnerSignup","outputs":[],"payable":false,"type":"function"},{"inputs":[],"type":"constructor","payable":true},{"anonymous":false,"inputs":[{"indexed":false,"name":"freelancerAddress","type":"address"},{"indexed":false,"name":"projectCost","type":"uint256"}],"name":"PaymentComplete","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"freelancerAddress","type":"address"},{"indexed":false,"name":"projectCost","type":"uint256"}],"name":"PaymentFailed","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"projectId","type":"uint256"},{"indexed":false,"name":"projectCost","type":"uint256"}],"name":"NewProjectPublished","type":"event"}]).at(ContractAddress);
      //
      // var blockchain = web3.eth;
      // myContract =  blockchain.contract(ABIArray).at(ContractAddress);
      return blanceContract;
    }
