
import { Users } from '../../common/api/api.js';


Template.users.helpers({
  users() {
    return Users.find({ }, { sort: { createdAt: -1 } });
  },
});
