import { Gigs } from '../../common/api/api.js';
Session.set("homeProjectGigState", 1);

Template.gig.helpers({
  isOpen() {
    var isOpen = false;
    if(1== this.projectState){
      isOpen = true;
    }
    return isOpen;
  },
});


Template.homeGigs.helpers({
  gigs() {
    // Show newest tasks at the top

    var searchCriteria = Session.get("homeProjectGigState") || {};
    console.log("search criteria " + searchCriteria);
    if(searchCriteria >= 0){
      return Gigs.find({ "projectState": searchCriteria }, { sort: { createdAt: -1 } });
    }else{
      // this is for all
      console.log("search All ");

      return Gigs.find({ }, { sort: { createdAt: -1 } });
    }

    // return Gigs.find({ "projectState": 0  }, { sort: { createdAt: -1 } });
  },
});

Template.homeGigs.events({
  'click .waiting': function(event) {
    event.preventDefault();
    Session.set("homeProjectGigState", 2);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".waiting").addClass("active");
  },
  'click .all': function(event) {
    event.preventDefault();
    Session.set("homeProjectGigState", -1);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".all").addClass("active");
  },
  'click .approved': function(event) {
    event.preventDefault();
    Session.set("homeProjectGigState", 3);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".approved").addClass("active");
  },
  'click .completed': function(event) {
    event.preventDefault();
    Session.set("homeProjectGigState", 4);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".completed").addClass("active");
  },
  'click .open': function(event) {
    event.preventDefault();
    Session.set("homeProjectGigState", 1);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".open").addClass("active");

  },
});

// 'onclick #searchBtn':function()
// {
//     Session.set("searchCriteria", {'Headline': search});
// }


Template.gigModal.helpers({
  authInProcess: function() {
    return Meteor.loggingIn();
  },
  canShow: function() {
    return !!Meteor.user();
  },
});

Template.gigModal.events({
  'click .confirm-accept': function(event) {
    event.preventDefault();
    $('#acceptModal').hide();
    var gigId =$(".confirm-accept").attr("id");
    console.log("id - " + gigId);

    Gigs.update(gigId, {
      $set: {
        projectState: 2,
        gigFreelancer: Meteor.userId()
      },
    });

    $('#acceptModalAction').show();
  },
});
//
// function accordation(id) {
//   console.log("accordation " + id);
//   var x = document.getElementById(id);
//   if (x.className.indexOf("w3-show") == -1) {
//     x.className += " w3-show";
//   } else {
//     x.className = x.className.replace(" w3-show", "");
//   }
// }

// Meteor.methods({
//   // methods go here
//   'accordations': function(){
//     console.log("Hello world");
//     // this method's function
//   }
// });
//
Template.gig.events({
  'click .gig-description': function(event) {
    event.preventDefault();
    // Meteor.call(accordations);
    $("#"+this._id).find(".gig-description-content").toggle();
  },
  'click .acceptButton': function(event) {
    event.preventDefault();
    // Meteor.call(accordations);
    // only logged in user would have this
    if(!!Meteor.user()){

      $("#acceptModalTitle").text(this.title);
      $("#acceptModalDescription").text(this.description);
      $("#gigCategory").text(this.category);
      $("#gigOwner").text(this.category);
      $("#gigCompletionDate").text(this.completionDate);
      $("#gigCost").text(this.budget);
      $(".confirm-accept").attr("id",""+this._id);
    }

    $("#acceptModal").show();
  }
});
