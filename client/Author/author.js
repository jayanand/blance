import { Gigs } from '../../common/api/api.js';
import { Users } from '../../common/api/api.js';
import { Categories } from '../../common/api/api.js';

Session.set("authorProjectGigState", -1);

Session.set("userName", "");
Session.set("userEmail", "");
Session.set("userType", -1);
Session.set("userId", "");

Template.authorProfile.helpers({
  myProfile() {
    return Users.find({ userId:Meteor.userId()});
  },
  categoryList() {
    return Categories.find({});
  },
});

Template.authorContact.helpers({
  myProfile() {
    return Users.find({ userId:Meteor.userId()});
  },
});

Template.authorProfile.events({
  'submit': function(event) {
    event.preventDefault();

    var userName = event.target.userName.value;
    var profilepic = event.target.profilepic.value;
    var country = event.target.country.value;
    var timezone = event.target.timezone.value;
    var description = event.target.description.value;
    var userPhone = event.target.userPhone.value;


    console.log("this._id " + this._id);

    Users.update(this._id, {
      $set: {
        userName: userName,
        profilePicUrl: profilepic,
        country: country,
        timeZone:timezone,
        description:description,
        userPhone:userPhone,
      },
    });

  },

});


Template.authorBody.helpers({
  users() {
    return Users.find({ }, { sort: { createdAt: -1 } });
  },
});

Template.authorPortfolio.helpers({
  filteredAuthorGigs() {
    var searchCriteria = Session.get("authorProjectGigState") || {};
    if(searchCriteria == 1){
      return Gigs.find({ "projectState": searchCriteria }, { sort: { createdAt: -1 } });
    } else if(searchCriteria >= 0){
      return Gigs.find({ gigFreelancer: Meteor.userId(), "projectState": searchCriteria }, { sort: { createdAt: -1 } });
    }else{
      return Gigs.find({ gigFreelancer: Meteor.userId() }, { sort: { createdAt: -1 } });
    }
  },
});

Template.authorGig.helpers({
  isOpen() {
    var isOpen = false;
    if(1== this.projectState){
      isOpen = true;
    }
    return isOpen;
  },
  isApproved() {
    var isOpen = false;
    if(3== this.projectState){
      isOpen = true;
    }
    return isOpen;
  },
});


//1 for open state, 2 for approval state, 3 for approved state,4 delevered,5 accepted
Template.authorHead.events({
  'click .author-all': function(event) {
    event.preventDefault();
    Session.set("authorProjectGigState", -1);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-all").addClass("active");
  },
  'click .author-waiting': function(event) {
    event.preventDefault();
    Session.set("authorProjectGigState", 2);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-waiting").addClass("active");
  },
  'click .author-approved': function(event) {
    event.preventDefault();
    Session.set("authorProjectGigState", 3);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-approved").addClass("active");
  },
  'click .author-completed': function(event) {
    event.preventDefault();
    Session.set("authorProjectGigState", 4);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-completed").addClass("active");
  },
  'click .author-open': function(event) {
    event.preventDefault();
    Session.set("authorProjectGigState", 1);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-open").addClass("active");

  },
  'click .author-paid': function(event) {
    event.preventDefault();
    Session.set("authorProjectGigState", 5);
    $(".filtergigs").each(function(){
      $(this).removeClass("active");
    });
    $(".author-paid").addClass("active");

  },
});


Template.authorGig.events({
  'click .gig-description': function(event) {
    event.preventDefault();
    // Meteor.call(accordations);
    $("#"+this._id).find(".gig-description-content").toggle();
  },
  'click .acceptButton': function(event) {
    event.preventDefault();
    // Meteor.call(accordations);
    // only logged in user would have this
    if(!!Meteor.user()){

      $("#acceptModalTitle").text(this.title);
      $("#acceptModalDescription").text(this.description);
      $("#gigCategory").text(this.category);
      $("#gigOwner").text(this.category);
      $("#gigCompletionDate").text(this.completionDate);
      $("#gigCost").text(this.budget);
      $(".confirm-accept").attr("id",""+this._id);
    }

    $("#acceptModal").show();
  },
  'click .deliverButton': function(event) {
    event.preventDefault();
    console.log("deliverGigModalTitle");
    if(!!Meteor.user()){
      $("#deliverGigModalTitle").text(this.title);
      $(".confirm-deliver").attr("id",""+this._id);
    }

    $("#deliverGigModal").show();
  },
  'click .detailsButton': function(event) {
    event.preventDefault();
    // Meteor.call(accordations);
    // only logged in user would have this
    if(!!Meteor.user()){
      var userObject = Users.find({ userId:this.gigOwner}).fetch()[0];

      $("#detailsModalTitle").text(this.title);
      $("#detailsModalDescription").text(this.description);
      $("#detailsModalVisitProfile").attr("href", "/aboutme/"+this.gigOwner);

      $("#gigWorth").text(this.budget);
      $("#gigDeadline").text(this.completionDate);
      $("#gigCategory").text(this.completiondate);
      $("#ownerCrediblity").text(this.crediblityFactor);
      $("#ownerCrediblity").text(this.crediblityFactor);
      $("#publisherGigs").text(this.totalGigsDelivered);

      $(".confirm-approve").attr("id",""+this._id);
      $(".confirm-decline").attr("id",""+this._id);

    }

    $("#detailsModal").show();
  },
});

Template.deliverGigModal.events({
  'click .confirm-deliver': function(event) {
    event.preventDefault();
    $('#deliverGigModal').hide();
    var gigId =$(".confirm-deliver").attr("id");
    var content =$("#deliverGigModalContent").val();
    console.log("content - " + content);

    console.log("id - " + gigId);
    Gigs.update(gigId, {
      $set: {
        projectState: 4,
        gigFreelancer: Meteor.userId(),
        gigContent: content
      },
    });

    $('#acceptModalAction').show();
  },
});



Template.authornav.events({
  'click .logout': function(event) {
    event.preventDefault();
    Meteor.logout();
  }
});

function isEmpty(str) {
  return (!str || 0 === str.length);
}

Template.authornav.helpers({
  userName() {
    return Session.get("userName");
  },
  userEmail() {
    return Session.get("email");
  },
  userPic() {
    var profileP = Session.get("profilePicUrl");

    if(isEmpty(profileP)){
      profileP= "http://santetotal.com/wp-content/uploads/2014/05/default-user.png";
    }
    return profileP;
  }
});
