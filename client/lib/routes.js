FlowRouter.route("/", {
	name: "Home",
	action() {
		if(!!Meteor.user()){
			BlazeLayout.render("blanceHome",{
				main: "Home"
			})
		}else{
			// BlazeLayout.render("profile",{
			// 	main: "Profile"
			// })
			FlowRouter.go("/Profile");
		}

	}
});

FlowRouter.route("/Home", {
	name: "Home",
	action() {
		BlazeLayout.render("blanceHome",{
			main: "Home"
		})
	}
});

FlowRouter.route("/Category", {
	name: "Category",
	action() {
		BlazeLayout.render("gigCategory",{
			main: "Category"
		})
	}
});


FlowRouter.route("/aboutme/:userId", {
	name: "AboutMe",
	action:function(params) {
		BlazeLayout.render("aboutme",{
			main: "AboutMe"
		})

		console.log("params.postId " + params.userId);
		FlowRouter.setParams({userId: params.userId});
	}
});

FlowRouter.route("/Profile", {
	name: "Profile",
	action() {
		BlazeLayout.render("profile",{
			main: "Profile"
		})
	}
});


FlowRouter.route("/Author", {
	name: "Author",
	action() {
		BlazeLayout.render("author",{
			main: "Author"
		})
	}
});


FlowRouter.route("/Publisher", {
	name: "Publisher",
	action() {
		BlazeLayout.render("publisher",{
			main: "Publisher"
		})
	}
});

FlowRouter.route("/Login", {
	name: "Login",
	action() {
		BlazeLayout.render("login",{
			main: "Login"
		})
	}
});


FlowRouter.route("/Signup", {
	name: "Signup",
	action() {
		BlazeLayout.render("signup",{
			main: "Signup"
		})
	}
});


FlowRouter.route("/PublisherLogin", {
	name: "PublisherLogin",
	action() {
		BlazeLayout.render("login",{
			main: "PublisherLogin"
		})
	}
});


FlowRouter.route("/PublisherSignup", {
	name: "PublisherSignup",
	action() {
		BlazeLayout.render("signup",{
			main: "PublisherSignup"
		})
	}
});


FlowRouter.route("/AuthorLogin", {
	name: "AuthorLogin",
	action() {
		BlazeLayout.render("login",{
			main: "AuthorLogin"
		})
	}
});


FlowRouter.route("/AuthorSignup", {
	name: "AuthorSignup",
	action() {
		BlazeLayout.render("signup",{
			main: "AuthorSignup"
		})
	}
});


FlowRouter.route("/GigForm", {
	name: "GigForm",
	action() {
		BlazeLayout.render("gigForm",{
			main: "GigForm"
		})
	}
});


FlowRouter.route("/Users", {
	name: "Users",
	action() {
		BlazeLayout.render("users",{
			main: "Users"
		})
	}
});
//
// FlowRouter.route('/profile/:postId', {
//     action: function(params, queryParams) {
//         console.log("Yeah! We are on the post:", params.postId);
//     }
// });
