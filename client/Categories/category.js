import { Categories } from '../../common/api/api.js';
Template.gigCategory.helpers({
  categoryList() {
    return Categories.find({});
  },
});

Template.categoryForm.events({
  'submit': function(event) {
    event.preventDefault();

    console.log("categoryForm click");
    var categoryName = event.target.categoryName.value;
    // Insert a task into the collection
    Categories.insert({
      categoryName:  categoryName,
      createdAt: new Date(), // current time
    },function(err,records){
      console.log("Record added as "+records[0]._id);
    });

    // Clear form
    event.target.categoryName.value = '';
  }
});
