pragma solidity ^0.4.0;

/**
Contract meant for doing escrow activities
*/
contract blance{

  address public blanceCreator;

  // strut to maintian Details of one project which will help us do some actions on same
  struct BlanceProject{
    uint projectId;  // this is the projectid of the the projects created.
    uint256 projectCost; // this is the escrow ether to recevved and to be sent to freelancer
    address freelancerAddress; // address of the freelancer to which we have to send the money across
    address ownerAddress; // address of the freelancer to which we have to send the money across
  }

  // array of projects
  mapping(uint => BlanceProject) public projects;

  function blance() public{
    blanceCreator = msg.sender;
  }
  uint projectsCount;

  function addProject(uint _ProjectId) public payable {
    projects[_ProjectId] =  BlanceProject({
      projectId:_ProjectId,
      projectCost:msg.value,
      freelancerAddress:0,
      ownerAddress:msg.sender
      });
      projectsCount++;
    }


    function addNewFreelancer(uint _projectId, address _FreelancerAddress) public {
      projects[_projectId].freelancerAddress = _FreelancerAddress;
    }

    function getFreelancer(uint _projectId) returns (address freelancerAddress){
      return projects[_projectId].freelancerAddress;
    }

    function getProjectCost(uint _projectId) returns (uint256 projectCost){
      return projects[_projectId].projectCost;
    }

    function payToFreelancer(uint _projectId) public {
      // local variable to hold amount to be sent, in case we have to revert when the transaction fails
      var amount = projects[_projectId].projectCost;

      if (amount > 0) {
        // It is important to set this to zero because the recipient
        // can calls this function again as part of the receiving call
        // before `send` returns.
        projects[_projectId].projectCost = 0;

        if (!projects[_projectId].freelancerAddress.send(amount)) {
          // No need to call throw here, just reset the amount owing
          projects[_projectId].projectCost = amount;
        }
      }


    }


  }
